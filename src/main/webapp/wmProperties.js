var _WM_APP_PROPERTIES = {
  "activeTheme" : "travel-advisor",
  "defaultLanguage" : "en",
  "displayName" : "TravelAssist",
  "homePage" : "Main",
  "name" : "TravelAssist",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};